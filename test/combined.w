Combined Test Script
=====================

.. test/combined.w

The combined test script runs all tests in all test modules.

@o test.py 
@{@<Combined Test overheads, imports, etc.@>
@<Combined Test suite which imports all other test modules@>
@<Combined Test main script@>
@}

The overheads import unittest and logging, because those are essential
infrastructure.  Additionally, each of the test modules is also imported.

@d Combined Test overheads...
@{"""Combined tests."""
import unittest
import test_loader
import test_tangler
import test_weaver
import test_unit
import logging
@}

The test suite is built from each of the individual test modules.

@d Combined Test suite...
@{
def suite():
    s= unittest.TestSuite()
    for m in ( test_loader, test_tangler, test_weaver, test_unit ):
        s.addTests( unittest.defaultTestLoader.loadTestsFromModule( m ) )
    return s
@}

The main script initializes logging. Note that the typical setup
uses ``logging.CRITICAL`` to silence some expected warning messages.
For debugging, ``logging.WARN`` provides more information.

Once logging is running, it executes the ``unittest.TextTestRunner`` on the test suite.


@d Combined Test main...
@{
if __name__ == "__main__":
    import sys
    logging.basicConfig( stream=sys.stdout, level=logging.CRITICAL )
    tr= unittest.TextTestRunner()
    result= tr.run( suite() )
    logging.shutdown()
    sys.exit( len(result.failures) + len(result.errors) )
@}